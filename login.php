<?php $titre = "Se connecter"; ?>
<?php ob_start();
session_start();
?>

<article>
    <h1>Se connecter</h1>
    <p>
    <form method="POST" action="login_action.php">
        <input type="text" name="login" placeholder="Saisissez votre login..." required>
        <input type="password" name="password" placeholder="Mot de passe" required>
        <input type="submit" value="Se connecter">
    </form>
    </p>

</article>

<?php
$contenu = ob_get_clean();
require 'gabarit/template.php'
?>