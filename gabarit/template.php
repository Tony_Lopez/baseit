<?php
require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeThemes = $objBdd->query("select * from theme");
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div id="container">
        <div class="login">
            <div class="titre">
                <h1>Base de connaisance IT</h1>
            </div>
        </div>
        <div class="connexion">
            <?php
            if (isset($_SESSION['logged_in']['login']) == TRUE) {
                //l'internaute est authentifié
                echo 'Salut à toi, ' . $_SESSION['logged_in']['pseudo'] . '! ';
                //affichage "se déconnecter"(logout.php), "prif", "paramètres", etc.
            ?>
                <br><br><a href="logout.php">Se déconnecter</a>
            <?php
            } else {
                //Personne n'est authentifié
                //affichage d'un lien pour se connecter
            ?>
                <a href="login.php">Connexion</a>
            <?php
            }
            ?>
        </div>
        <div class="themeDoc">
            <div class="theme">
                <h2>Theme</h2>
                <ul>

                    <li><a href="index.php">Accueil</a></li>

                    <?php foreach ($listeThemes as $theme) { ?>
                        <li><a href="theme.php?idTheme=<?= $theme['idTheme']; ?>">
                                <span><?= $theme['nom']; ?></span></a>
                        </li>
                    <?php } ?>

                </ul>
            </div>

            <?php if (isset($_SESSION['logged_in']['login']) == TRUE) { ?>
                <li><a href="ajout-article.php">Ajouter un Article</a></li>
                <li><a href="suppr-article.php">Supprimer un Article</a></li>
            <?php } ?>

            <div class="doc">
                <?php echo $contenu ?>
            </div>
        </div>
    </div>
</body>

</html>