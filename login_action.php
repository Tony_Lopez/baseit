<?php
//activer les sessions
session_start();
require 'bdd/bddconfig.php';
//Tester si les variables POST existent
$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}

//si login et password sont bien reçus
if ($paramOK == true) {
    //vérifier si le login passord est correct (base de données) : voir après
    // test d'affichage
    try {
        $objBdd = new PDO("mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8", $bddlogin, $bddpass);

        $PDOlistlogins = $objBdd->prepare("SELECT * FROM user WHERE login = :login ");
        $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
        $PDOlistlogins->execute();
        //S'il y a un résultat à la requête 
        $row_user = $PDOlistlogins->fetch();
        if ($row_user != false) {
            //il existe un login identique dans la base
            // vérif du password : voir après
            if (password_verify($password, $row_user['password'])) {
                //authentification réussie
                //création de la variable de session : voir après//tableau des données à stocker en session
                $session_data = array(
                    'idUser' => $row_user['idUser'],
                    'login' => $row_user['login'],
                    'pseudo' => $row_user['pseudo'],
                );
                //régénérer le session id
                session_regenerate_id();
                //enregistrer les données dans une variable de session
                $_SESSION['logged_in'] = $session_data;
                $PDOlistlogins->closeCursor();
                /* Redirige vers la page d'accueil */
                $serveur = $_SERVER['HTTP_HOST'];
                $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
                $page = 'index.php';
                header("Location: http://$serveur$chemin/$page");
            } else {
                //Mauvais password
                session_destroy();
                die('Authentification incorrecte');
            }
        } else {
            //Mauvais login
            session_destroy();
            die('Authentification incorrecte');
        }
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
} else {
    die('Vous devez fournir un login et un mot de passe');
}
