<?php
$idTheme = 0;
if (isset($_GET['idTheme'])) {
    $idTheme = intval(htmlspecialchars($_GET['idTheme']));
}
require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $pdoStmt = $objBdd->prepare("SELECT * FROM article,user WHERE idTheme = :id AND article.idUser = user.idUser");
    $pdoStmt->bindParam(':id', $idTheme, PDO::PARAM_INT);
    $pdoStmt->execute();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<?php $titre = "Accueil"; ?>
<?php ob_start();
session_start();
?>
<ul>
    <?php foreach ($pdoStmt as $article) { ?>
        <li><a href="article.php?idArticle=<?= $article['idArticle']; ?>">
                <span><?= $article['titre']; ?>auteur: <?= $article['pseudo']; ?> - <?= $article['datePub']; ?> - </span></a>
        </li>
    <?php } ?>
</ul>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>