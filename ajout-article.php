<?php
require("bdd/bddconfig.php");
$objBdd = new PDO("mysql:host=$bddserver;
dbname=$bddname;
charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$pdoStmt = $objBdd->prepare("SELECT * FROM theme");
$pdoStmt->execute()
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- formulaire
    - theme
    - nom
    - article
    - 1 seul lien
    - 1 autre lien
    - + cliquez pour ajouter un lien (javascript)
    <input type="text" class="lien" name="lien1" id="" value="htpp://sxccwcw">
    <input type="text" class="lien" name="lien2" id="" value="htpp://sxccwcw">

    envoi du formulaire en js apres avoir créer une variable POST ; tableau des liens
    tableau des liens en JSON :
    {
    "idTheme": 1,
    "nomArticle": "la recette des tartes aux pommes",
    "texte": "gjjgg,cfj,fjgfj,v",
    "liens": [
    {"lien": "htpp://aaaaaa"},
    {"lien": "htpp://bbbbb"}
    ]
    "photos": [
    {"lien": "htpp://aaaaaa"},
    {"lien": "htpp://bbbbb"}
    ],
    }

    Front end : <HTML>

    </HTML> javascript
    réseau en JSON
    Back end <php json_encode() ?>

        en <?php
            $_POST['lien1'];
            $_POST['lien2']
            ?> -->

</body>

</html>

<?php $titre = "Ajouter un article"; ?>
<?php ob_start(); ?>

<article>
    <?php
    session_start();
    //Accès seulement si authentifié
    if (isset($_SESSION['logged_in']['login']) !== TRUE) {
        // Redirige vers la page d'accueil (ou login.php) si pas authentifié
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");
    }
    ?>
    <h1>Ajouter un article</h1>
    <form method="POST" action="insert_article.php">
        <fieldset>
            <legend>Nouvel article<?php echo $_SESSION['logged_in']['idUser']; ?></legend>
            Nom :<br />
            <input type="text" name="titre" value="" placeholder="Nom de l'article" required>
            <br />
            Description :<br>
            <textarea name="texte" rows="10" cols="40" placeholder="Description de l'article" required></textarea>
            <br />
            <select name="theme">
                <?php
                while ($theme = $pdoStmt->fetch()) {
                ?>

                    <option value="<?php echo $theme["idTheme"] ?>"><?php echo $theme["nom"] ?></option>

                <?php
                };
                $pdoStmt->closeCursor();
                ?>
            </select>
            <input type="submit" value="Enregistrer">
        </fieldset>
    </form>
</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>