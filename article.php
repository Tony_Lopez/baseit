<?php

$idArticle = 0;
if (isset($_GET['idArticle'])) {
    $idArticle = intval(htmlspecialchars($_GET['idArticle']));
} else {
    die("Vous devez saisir un article");
}

require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeArticle = $objBdd->prepare("SELECT * FROM article WHERE idArticle = :id");
    $listeArticle->bindParam(':id', $idArticle, PDO::PARAM_INT);
    $listeArticle->execute();

    $listeLien = $objBdd->prepare("SELECT * FROM document WHERE idArticle = :id and type ='lien'");
    $listeLien->bindParam(':id', $idArticle, PDO::PARAM_INT);
    $listeLien->execute();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<?php $titre = "Acceuil"; ?>
<?php ob_start();
session_start();
?>

<?php foreach ($listeArticle as $article) { ?>
    <a <?= $article['idArticle']; ?>">
        <span>
            <h2><?= $article['titre']; ?></h2>
        </span>
        <span> <?php echo $article['texte']; ?></span></a>

<?php } ?>

<?php foreach ($listeLien as $lien) { ?>
    <a <?= $lien['idArticle']; ?>">
        <span><a href="<?= $lien['url']; ?>" target="_blank"><?= $lien['nom']; ?>"></span>
        <a href="http://gitlab.com" target="_blank">gitlab</a>

    <?php } ?>

    <?php $contenu = ob_get_clean(); ?>
    <?php require 'gabarit/template.php'; ?>