<?php

session_start();
if (isset($_SESSION['logged_in']['login']) !== TRUE) {
    // Redirige vers la page d'accueil (ou login.php) si pas authentifié
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
}

require 'bdd/bddconfig.php';
//Récuperer les 3 variables POST
//sécuriser les variables reçues
$paramOk = false;

if (isset($_POST['titre'])) {
    $titre = htmlspecialchars($_POST['titre']);

    if (isset($_POST['texte'])) {
        $texte = htmlspecialchars($_POST['texte']);
        $paramOk = true;
    }
}

if ($paramOk == true) {
    // INSERT dans la base
    try {
        $objBdd = new PDO("mysql:host=$bddserver;
    dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdoStmt = $objBdd->prepare("INSERT INTO article (titre, texte, idTheme, idUser) VALUES (:titre, :texte, :idTheme,
        :idUser)");
        $pdoStmt->bindParam(':titre', $titre, PDO::PARAM_STR);
        $pdoStmt->bindParam(':texte', $texte, PDO::PARAM_STR);
        $idTheme = $_POST['theme'];
        $pdoStmt->bindParam(':idTheme', $idTheme, PDO::PARAM_INT);
        $pdoStmt->bindParam(':idUser', $_SESSION['logged_in']['idUser'], PDO::PARAM_INT);
        $pdoStmt->execute();

        $lastId = $objBdd->lastInsertId();
    } catch (Exception $prmE) {
        die('Erreur:' . $prmE->getMessage());
    }

    //rediriger automatiquement dans la page bassins.php
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
} else {
    die('Les paramètres reçus ne sont pas valides');
}
